using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.DevTools.V102.Browser;
using OpenQA.Selenium.Interactions;
using System;

namespace Selen
{
    public class Tests
    {
        public IWebDriver driver;

        [SetUp]
        public void Setup()
        {
            ChromeOptions options = new ChromeOptions();
            options.AddArgument("--disable-blink-features");
            options.AddArgument("--disable-blink-features=AutomationControlled");

            driver = new ChromeDriver(options);

            driver.Manage().Window.Maximize();
        }

        [Test]
        public void Test1()

        {               
            driver.Navigate().GoToUrl("https://accounts.ukr.net/login");
            Thread.Sleep(1000);

            driver.FindElement(By.Name("login")).SendKeys("selwebtest@ukr.net");
            driver.FindElement(By.Name("password")).SendKeys("!webtest!");
            Thread.Sleep(1000);

            driver.FindElement(By.XPath("//button[contains(@class,'Ol0-ktls jY4tHruE')]")).Click();
            Thread.Sleep(2000);

            driver.FindElement(By.XPath("//button[text()='�������� �����']")).Click();
            Thread.Sleep(1000);

            driver.FindElement(By.Name("toFieldInput")).SendKeys("selwebtest@ukr.net");
            driver.FindElement(By.Name("subject")).SendKeys("Letter and nothing else");
                                                
            driver.FindElement(By.TagName("textarea")).SendKeys("Just a text");            
                       
            driver.FindElement(By.XPath("//div[@class='sendmsg__bottom-controls']//button[1]")).Click(); 
            
            Thread.Sleep(1000);

            //Assert.Pass();
            driver.Close();
        }
    }
}